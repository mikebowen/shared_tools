# Code created by Mike Bowen
# I am a Network Engineer for an active ethernet, fiber optic network
# Please let me know if you add or modify the code to make it better.
# I am always open for suggestions.


#    _                                     
#   | | ___   ___  _ __  ___   _ __  _   _ 
#   | |/ _ \ / _ \| '_ \/ __| | '_ \| | | |
#   | | (_) | (_) | |_) \__ \_| |_) | |_| |
#   |_|\___/ \___/| .__/|___(_) .__/ \__, |
#                 |_|         |_|    |___/ 

### basic info #
# This helps find loops on Alcatel Omniswitch 6250/6400/6450/6850
# First know it is meant for a database of devices to search through.
# The script telnets into the devices, collects the mac-address-table
# information including vlan, mac, slot/port, switch and creates a
# sqlite3 database that it dumps the information in.  it then does
# a sql query on the table to find loops and prints the info.
# Remember to delete your sqlite3 database after you fix the loop.
