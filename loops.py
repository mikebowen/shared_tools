#!/usr/bin/python

import telnetlib
import re
import socket
import time
import MySQLdb
import multiprocessing

####### VARIABLES ########

# change 192.168.1.2 to the ip of your server with the switch database on it.
db_ip = '192.168.1.2'
# change db_usrname to the user name of the switch database.
db_name = 'db_usrname'
# change db_passwd to the password of the switch database.
db_pass = 'db_passwd'
# change device_db to the name of the switch database.
db_db = 'device_db'
# set the select statment to the sql query to get a list of your switch ip addresses.
sql = "select ip from switch_table where switch_type like '6%' and status = 'active';"
# change tn_username to your telnet user name for the switches
user = "tn_username"
# change tn_passwd to your telnet password for the switches
password = "tn_passwd"
# change the directory to where you want your sqlite3 database to be stored
sqlite_db = '/root/test_4_'

##########################


def ads_6000_active():
  db = MySQLdb.connect(db_ip, db_name, db_pass, db_db )
  cursor = db.cursor()
  try:
    cursor.execute(sql)
    results = cursor.fetchall()
  except:
    db.rollback()
    print "error: unable to fetch data"
  db.close()
  return results

def ads_loops(self):
  import sqlite3 as lite
  import sys
  try:
    tn = telnetlib.Telnet(ip)
    tn.read_until("ogin : ")
    tn.write(user + "\n")
    tn.read_until("assword : ")
    tn.write(password + "\n")
    tn.read_until("-> ")
    tn.write("show mac-address-table" + "\n")
    tn.write("show mac-address-table" + "\n")
    time.sleep(1)
    tn.write("show mac-address-table" + "\n")
    tn.write("show mac-address-table" + "\n")
    tn.write("exit\n")
    output = tn.read_all()
    vlan = (re.findall('\s+(\d+)\s+[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}\s+', output))
    macs = (re.findall('([a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2}:[a-f0-9]{2})\s+learned', output))
    ports = (re.findall('bridging\s+(\d/\d+)', output))
    con = lite.connect(sqlite_db+ip+'_loops.db')
    with con:
      for h,i,j in zip(macs, ports, vlan):
        cur = con.cursor()    
        cur.execute("CREATE TABLE IF NOT EXISTS ads_loops_data(ads Text, vlan TEXT, port TEXT, mac TEXT)")
        cur.execute("INSERT INTO ads_loops_data VALUES('"+ip+"','"+j+"','"+i+"','"+h+"')")
      cursor = con.execute("select distinct * from ads_loops_data as t1, ads_loops_data as t2 where t1.mac = t2.mac and t1.port != t2.port") 
      for row in cursor:
        print "ads   =  ", row[0]
        print "vlan  =  ", row[1]
        print "port  =  ", row[2]
        print "mac   =  ", row[3], "\n"
  except socket.error:
    print "%s host could not be found" % (ip)


if __name__ == '__main__':
  ads_6000_active()
  jobs = []
  for count in ads_6000_active():
    ip = count[0]
    p = multiprocessing.Process(target=ads_loops, args=(ip,))
    jobs.append(p)
    p.start()
